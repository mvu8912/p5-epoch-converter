package Epoch2;
{
  $Epoch2::VERSION = '0.01';
}
use strict;
use warnings;
use DateTime;
use Date::Parse;
use base "Exporter";
our @EXPORT_OK = ("epoch2");

sub epoch2 {
    my $raw = shift;
    if ( !$raw ) {
        my $now = DateTime->now;
        return sprintf "%d = %s %s\n", $now->epoch, $now->ymd, $now->hms;
    }
    if ( $raw !~ /^\d+$/ ) {
        $raw = Date::Parse::str2time($raw);
    }
    if ( !$raw ) {
        return "Invalid Timestamp or date time string\n";
    }
    if ( $raw =~ /^\d+$/ ) {
        my $dt = DateTime->from_epoch( epoch => $raw );
        return sprintf "%d = %s %s\n", $dt->epoch, $dt->ymd, $dt->hms;
    }
}

1;
